import React, { useState } from "react";
import ifcIcon from "../../static/images/logos/influenceIcon.svg";
import CoinSelectModal from "../SelectCountry/CoinSelectModal";

function TransactionCardHome({ coin, setCoin, coinList }) {
  const [tab, setTab] = useState("Stake");
  const [openModal, setOpenModal] = useState(false);
  return (
    <>
      <div className="transactionCard">
        <div className="tabs">
          <div className={`tab ${tab === "Buy"}`} onClick={() => setTab("Buy")}>
            Buy
          </div>
          <div
            className={`tab ${tab === "Stake"}`}
            onClick={() => setTab("Stake")}
          >
            Stake
          </div>
          <div className={`tab ${tab === "Win"}`} onClick={() => setTab("Win")}>
            Win
          </div>
        </div>
        <div className="contents">
          <div className="group">
            <div className="label">You Are Spending</div>
            <label className="inpWraper">
              <input type="text" placeholder={`${coin?.symbol || "$"}0.00`} />
              <div className="coin btns" onClick={() => setOpenModal(true)}>
                <img src={coin?.coinImage} alt="" />
                <span>{coin?.coinSymbol}</span>
              </div>
            </label>
          </div>
          <div className="group">
            <div className="label">You Are Receiving</div>
            <label className="inpWraper">
              <input type="text" placeholder="1.000" />
              <div className="coin">
                <img src={ifcIcon} alt="" />
                <span>IFC</span>
              </div>
            </label>
          </div>
        </div>
        <div className="ftBtns">
          <div className="btFees">Fees</div>
          <div className="btBuy">Buy</div>
        </div>
      </div>
      {openModal && (
        <CoinSelectModal
          coinList={coinList}
          setCoin={setCoin}
          onClose={() => setOpenModal(false)}
        />
      )}
    </>
  );
}

export default TransactionCardHome;
