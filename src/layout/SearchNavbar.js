import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useEffect, useRef } from "react";
import { useHistory } from "react-router-dom";

import cryptoBankLogo from "../static/images/logos/influenceCoin.svg";
import cryptoBankLogoWhite from "../static/images/logos/influenceCoin.svg";
import ieIcon from "../static/images/offeringIcons/ieIcon.svg";
import ifIcon from "../static/images/offeringIcons/ifIcon.svg";

function SearchNavbar({ invert, type, setType, search, setSearch, onClick }) {
  const history = useHistory();
  const ref = useRef();
  useEffect(() => {
    ref.current && ref.current.focus();
  }, []);
  return (
    <nav className="searchNavbar">
      <img
        src={invert ? cryptoBankLogoWhite : cryptoBankLogo}
        alt="cryptobanklogo"
        className="crypto-img"
        onClick={() => {
          try {
            onClick();
          } catch (error) {}
          history.push("/");
        }}
      />
      <label className="navSearch">
        <input
          ref={ref}
          type="text"
          className="textInput"
          placeholder={`Search For An ${
            type === "ieo" ? "Offering" : "Contract"
          }`}
          value={search}
          onChange={(e) => setSearch(e.target.value)}
        />
        <FontAwesomeIcon icon={faSearch} className="searchIcn" />
        <div
          className={`icon ${type === "ifo"}`}
          onClick={() => setType("ifo")}
        >
          <img src={ifIcon} alt="" />
        </div>
        <div
          className={`icon ${type === "ieo"}`}
          onClick={() => setType("ieo")}
        >
          <img src={ieIcon} alt="" />
        </div>
      </label>
    </nav>
  );
}

export default SearchNavbar;
