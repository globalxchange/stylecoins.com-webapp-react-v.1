import React, { useContext, useEffect, useState } from "react";
import { MainLayout, MainNavbar, FooterFlipCards } from "@teamforce/coins-app";
import { useHistory } from "react-router";
import axios from "axios";
import moment from "moment";

import logo from "../static/images/logos/influenceCoin.svg";
import logoIcon from "../static/images/logos/influenceIcon.svg";
import ChatsIoComponentM from "../components/ChatsIoComponent/ChatsIoComponent";
import { ChatsContext } from "../context/ChatsContext";
import useWindowDimensions from "../Utils/WindowSize";
import profiles from "../static/images/logos/profiles.svg";
import oppertunities from "../static/images/logos/oppertunities.svg";
import terminals from "../static/images/logos/terminals.svg";
import virtualProspectus from "../static/images/logos/virtualProspectus.svg";
import locationPin from "../static/images/slider/pin.svg";

import TransactionCardHome from "../components/TransactionCardHome/TransactionCardHome";
import CoinSelectModal from "../components/SelectCountry/CoinSelectModal";

function HomePage() {
  const history = useHistory();
  const { chatOn, setChatOn } = useContext(ChatsContext);
  const { height } = useWindowDimensions();

  const [openModal, setOpenModal] = useState(false);
  const [coinList, setCoinList] = useState([]);
  const [coin, setCoin] = useState();
  const [coinBuy, setCoinBuy] = useState();
  const [slider, setSlider] = useState(17);

  useEffect(() => {
    axios
      .get("https://comms.globalxchange.com/coin/vault/get/all/coins")
      .then(({ data }) => {
        if (data.status) {
          setCoinList(data.coins);
          const cCoin = data.coins.filter(
            (coin) => coin.coinSymbol === "USD"
          )[0];
          setCoin(cCoin);
          setCoinBuy(cCoin);
        }
      });
  }, []);

  return (
    <MainLayout
      chatOn={chatOn}
      chatComponent={<ChatsIoComponentM />}
      primaryColor=""
      appColor=""
    >
      <div className="landingPage" style={{ height }}>
        <MainNavbar
          className=""
          logo={logo}
          onLogoClick={() => history.push("/")}
          chatOn={chatOn}
          setChatOn={setChatOn}
          btIcon={logoIcon}
          onBtClick={() => {
            history.push("/InfluenceApps");
          }}
          btLabel="InfluenceApps"
        />
        <div className="pageContents">
          <div className="titleDesc">
            <div className="titleSm">Your Network Is Your</div>
            <div className="title">Net-Worth. Literally</div>
            <p>
              Influence (IFC) Is The Worlds First Cryptocurrency That Quantifies
              The Value Of Your Network.
            </p>
            <div className="btWrap">
              <div className="head">
                <span>Today</span>
                <span>{moment().format("MMM Do YYYY")}</span>
              </div>
              <div className="btRates">
                <div className="coin" onClick={() => setOpenModal(true)}>
                  <img src={coin?.coinImage} alt="" />
                  <span>{coin?.coinSymbol}</span>
                </div>
                <span className="value">
                  {coin?.symbol}0.004
                  <small>+(2.31%)</small>
                </span>
              </div>
            </div>
            <div className="sliderWrapper">
              <img
                src={locationPin}
                alt=""
                className="locPin"
                style={{
                  left: `${slider}%`,
                  right: `${100 - slider}%`,
                }}
              />
              <input
                type="range"
                className="range"
                min={0}
                max={100}
                onChange={(e) => setSlider(e.target.value)}
              />
              <div className="labels">
                <div className="creation">Creation</div>
                <div className="launch">Launch</div>
                <div className="lock">Lock</div>
              </div>
              <div className="redeem">Redeem</div>
            </div>
          </div>
          <div className="cardWrapper">
            <TransactionCardHome
              coin={coinBuy}
              setCoin={setCoinBuy}
              coinList={coinList}
            />
          </div>
        </div>
        <FooterFlipCards
          footerCards={[
            {
              label: "Prospectus",
              logo: virtualProspectus,
            },
            {
              label: "Trading",
              logo: terminals,
            },
            {
              label: "Partnerships",
              logo: oppertunities,
            },
            {
              label: "Background",
              logo: profiles,
            },
          ]}
        />
      </div>
      {openModal && (
        <CoinSelectModal
          coinList={coinList}
          setCoin={setCoin}
          onClose={() => setOpenModal(false)}
        />
      )}
    </MainLayout>
  );
}

export default HomePage;
